import numpy as np
import csv
from tissueviewer.tvtiff import tiffread, tiffsave
import cPickle

__doc__ = """
Cosntanza format segmentation background is -1
MGX format segmentation background is 0
TissueViewer format segmentation background is 1
"""

def shiftSegmentationLabels(imageFileName, shift):
    image, tags = tiffread(imageFileName)
    map = np.array([i + shift for i in xrange(np.max(image) + 1)])
    newImage = map[image]
    tiffsave(newImage, imageFileName[:-4] + "_shifted.tif", **tags)
    

def shiftMGXTracking(csvFileName, shift):
    newDaughterMothers = []
    with open(csvFileName) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        counter = 0
        for row in readCSV:
            if counter > 0:
                daughter, mother = int(row[0]), int(row[1])
                print row, daughter, mother
                newDaughterMothers.append((daughter + shift, mother + shift))
            counter += 1
    
    fobj = file(csvFileName[:-4] + "shifted.csv", "w")
    fobj.write("Label, Parent Label\n")
    counter = 0
    for p in newDaughterMothers:
        counter += 1
        if counter < len(newDaughterMothers):
            fobj.write("%d, %d\n"%(p[0], p[1]))
        else:
            fobj.write("%d, %d"%(p[0], p[1]))
    fobj.close()
    
def writeTissueViewerInMGXTrackingFormat(tvPickleFile):
    fobj = file(tvPickleFile)
    data = cPickle.load(fobj)
    fobj.close()
    daughterMothers = data[0]
    fobj = file(tvPickleFile[:-4] + ".csv", "w")
    fobj.write("Label, Parent Label\n")
    counter = 0
    for p in daughterMothers:
        counter += 1
        if counter < len(daughterMothers):
            fobj.write("%d, %d\n"%(p[0], p[1]))
        else:
            fobj.write("%d, %d"%(p[0], p[1]))
    fobj.close()
    

def writeTissueViewerTrackingFileInTextFile(tvPickleFile, shift = 0):
    fobj = file(tvPickleFile)
    data = cPickle.load(fobj)
    fobj.close()
    daughterMothers = data[0]
    fobj = file(tvPickleFile[:-4] + ".txt", "w")
    fobj.write("Label, Parent Label\n")
    counter = 0
    for p in daughterMothers:
        counter += 1
        if counter < len(daughterMothers):
            fobj.write("%d, %d\n"%(p[0] + shift, p[1] + shift))
        else:
            fobj.write("%d, %d"%(p[0] + shift, p[1] + shift))
    fobj.close()    

def shiftMGXTrackingWriteInTXT(csvFileName, shift):
    newDaughterMothers = []
    with open(csvFileName) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        counter = 0
        for row in readCSV:
            if counter > 0:
                daughter, mother = int(row[0]), int(row[1])
                print row, daughter, mother
                newDaughterMothers.append((daughter + shift, mother + shift))
            counter += 1
    
    fobj = file(csvFileName[:-4] + "shifted.txt", "w")
    fobj.write("Label, Parent Label\n")
    counter = 0
    for p in newDaughterMothers:
        counter += 1
        if counter < len(newDaughterMothers):
            fobj.write("%d, %d\n"%(p[0], p[1]))
        else:
            fobj.write("%d, %d"%(p[0], p[1]))
    fobj.close()
    

def writeALTTrackingFormatIntoMGX(altTrackingFileName, shift = -1):
    fobj = file(altTrackingFileName)
    daughterMothers = []
    for line in fobj:
        l = line.split(": ")
        mother = int(l[0])
        daughters = [int(item) for item in l[1].split(", ")]
        print mother, daughters
        for d in daughters:
            daughterMothers.append((d, mother))
    fobj.close()
    
    fobj = file(altTrackingFileName[:-4] + ".csv", "w")
    fobj.write("Label, Parent Label\n")
    counter = 0
    for p in daughterMothers:
        counter += 1
        if counter < len(daughterMothers):
            fobj.write("%d, %d\n"%(p[0], p[1]))
        else:
            fobj.write("%d, %d"%(p[0], p[1]))
    fobj.close()

 



    



